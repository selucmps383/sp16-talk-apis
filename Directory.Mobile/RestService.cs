﻿using System;
using RestSharp.Portable.HttpClient;
using System.Threading.Tasks;
using RestSharp.Portable;
using System.Collections.Generic;

namespace Directory.Mobile
{
    public class ApiService
    {
        private readonly RestClient client;

        public ApiService()
        {
            client = new RestClient("http://10.211.55.3/api/");
        }

        public async Task<List<T>> Get<T>(string resource)
        {
            var request = new RestRequest(resource, Method.GET);
            var response = await client.Execute<List<T>>(request);
            return response.Data;
        }

        public async Task Post<T>(string resource, T item)
        {
            var request = new RestRequest(resource, Method.POST);
            request.AddBody(item);
            await client.Execute(request);
        }

        public async Task Delete(string resource, int id)
        {
            var request = new RestRequest(resource + "/" + id, Method.DELETE);
            await client.Execute(request);
        }
    }
}

