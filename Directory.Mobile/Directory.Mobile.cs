﻿using Xamarin.Forms;
using Directory.Mobile.Pages;

namespace Directory.Mobile
{
    public class App : Application
    {
        public App()
        {
            MainPage = new NavigationPage(new ListPage());
        }
    }
}