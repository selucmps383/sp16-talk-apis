﻿using Xamarin.Forms;
using Directory.Core;

namespace Directory.Mobile.Pages
{
    public class DetailPage : ContentPage
    {
        private readonly ApiService apiService;
        private readonly Person person;

        public DetailPage(Person person)
        {
            apiService = new ApiService();
            this.person = person;

            SetUpView();
        }

        private void SetUpView()
        {
            Title = person.FullName;

            var btnDelete = new Button
            {
                Text = "Delete"
            };
            btnDelete.Clicked += DeleteClicked;

            Content = new ContentView
            {
                Content = new StackLayout
                {
                    Padding = 10,
                    VerticalOptions = LayoutOptions.Start,
                    HorizontalOptions = LayoutOptions.Center,
                    Children =
                    {
                        new StackLayout
                        {
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            { 
                                new Label { Text = "First Name: " },
                                new Label { Text = person.FirstName }
                            }
                        },
                        new StackLayout
                        {
                            Orientation = StackOrientation.Horizontal,
                            Children =
                            { 
                                new Label { Text = "Last Name: " },
                                new Label { Text = person.LastName }
                            }
                        },
                        btnDelete
                    }
                }
            };
        }

        private async void DeleteClicked(object sender, System.EventArgs e)
        {
            await apiService.Delete("person", person.Id);
            Navigation.PopAsync();
        }
    }
}

