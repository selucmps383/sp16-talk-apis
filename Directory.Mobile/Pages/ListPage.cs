﻿using System;
using Xamarin.Forms;
using Directory.Core;

namespace Directory.Mobile.Pages
{
    public class ListPage : ContentPage
    {
        private readonly ApiService apiService;
        private ListView listView;

        public ListPage()
        {
            apiService = new ApiService();
            SetUpView();
        }

        protected override void OnAppearing()
        {
            GetPeople();
        }

        private void SetUpView()
        {

            listView = new ListView();
            listView.ItemTapped += (sender, e) => Navigation.PushAsync(new DetailPage((Person)e.Item));

            var btnAdd = new ToolbarItem
            {
                Text = "Add"
            };
            btnAdd.Clicked += (sender, e) => Navigation.PushAsync(new AddPage());

            ToolbarItems.Add(btnAdd);
            Content = listView;
        }

        private async void GetPeople()
        {
            var template = new DataTemplate(typeof(TextCell));
            template.SetBinding(TextCell.TextProperty, "FullName");

            listView.ItemsSource = await apiService.Get<Person>("person");
            listView.ItemTemplate = template;
        }
    }
}

