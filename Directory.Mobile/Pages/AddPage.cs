﻿using System;
using Xamarin.Forms;
using Directory.Core;

namespace Directory.Mobile.Pages
{
    public class AddPage : ContentPage
    {
        private readonly ApiService apiService;
        private Entry txtFirstName, txtLastName;

        public AddPage()
        {
            apiService = new ApiService();
            SetUpView();
        }

        private void SetUpView()
        {
            Title = "Add Person";

            txtFirstName = new Entry
            {
                Placeholder = "First Name"
            };
            txtLastName = new Entry
            {
                Placeholder = "Last Name"
            };
                
            var btnSave = new Button
            {
                Text = "Add Person"
            };
            btnSave.Clicked += SaveClicked;

            Content = new ContentView
            {
                Padding = 20,
                Content = new StackLayout
                {
                    VerticalOptions = LayoutOptions.Start,
                    Children = { txtFirstName, txtLastName, btnSave }
                }
            };
        }

        private async void SaveClicked(object sender, EventArgs e)
        {
            var person = new Person
            {
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text
            };

            await apiService.Post("person", person);

            Navigation.PopAsync();
        }
    }
}

